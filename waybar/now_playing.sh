#!/bin/sh

playerctl metadata --format '{"text": "{{artist}} - {{title}}", "tooltip": "{{album}}", "class": "media"}' --follow
