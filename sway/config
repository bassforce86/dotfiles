### Variables

# Logo key. Use Mod1 for Alt.
set $mod Mod4

# Terminal emulator
set $term alacritty

# Launcher
set $menu flock --nonblock /tmp/.wofi.lock  wofi --show drun | xargs swaymsg exec --
bindsym Menu exec $menu

### Output configuration

# Wallpaper
output * bg /home/james/.config/wallpapers/wall1.jpg fill

### Key bindings

## Basics
# Start a terminal
bindsym $mod+Return exec $term

# Kill focused window
bindsym $mod+Shift+q kill

floating_modifier $mod normal

# Reload the configuration file
bindsym $mod+Shift+c reload

# Exit sway (logs you out of your Wayland session)
bindsym $mod+Shift+e exec wlogout

### Moving around
# Change Focus
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Move the focused window
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right


# Workspaces

# Switch to workspace
bindsym $mod+1 workspace number 1
bindsym $mod+2 workspace number 2
bindsym $mod+3 workspace number 3
bindsym $mod+4 workspace number 4
bindsym $mod+5 workspace number 5
bindsym $mod+6 workspace number 6
bindsym $mod+7 workspace number 7
bindsym $mod+8 workspace number 8
bindsym $mod+9 workspace number 9
bindsym $mod+0 workspace number 10
# Move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number 1
bindsym $mod+Shift+2 move container to workspace number 2
bindsym $mod+Shift+3 move container to workspace number 3
bindsym $mod+Shift+4 move container to workspace number 4
bindsym $mod+Shift+5 move container to workspace number 5
bindsym $mod+Shift+6 move container to workspace number 6
bindsym $mod+Shift+7 move container to workspace number 7
bindsym $mod+Shift+8 move container to workspace number 8
bindsym $mod+Shift+9 move container to workspace number 9
bindsym $mod+Shift+0 move container to workspace number 10

### Layout

## Splitting Focused Window
# horizontal
bindsym $mod+b splith
# vertical
bindsym $mod+v splitv

# Switch Focused Window Layout Style
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Make the current focus fullscreen
bindsym $mod+f fullscreen

# Toggle the current focus between tiling and floating mode
bindsym $mod+Shift+space floating toggle

# Swap focus between the tiling area and the floating area
bindsym $mod+space focus mode_toggle

# Move focus to the parent container
bindsym $mod+a focus parent

### Scratchpad

# Move the currently focused window to the scratchpad
bindsym $mod+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+minus scratchpad show

### Other

# Resize
mode "resize" {
    
    # Width
    bindsym Left resize shrink width 10px
    bindsym Right resize grow width 10px
    # Height
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

# Status Bar
bar {
    swaybar_command waybar
    workspace_buttons yes
}

# Input Config
input type:keyboard {
   xkb_layout gb(extd)
}

# Media Keys
bindsym --locked XF86AudioPlay exec playerctl play-pause
bindsym --locked XF86AudioNext exec playerctl next
bindsym --locked XF86AudioPrev exec playerctl previous
bindsym --locked XF86AudioMute exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym --locked XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym --locked XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +5%

# display
gaps top 0
gaps left 5
gaps right 5
gaps bottom 5
gaps inner 5

default_border pixel 2
default_floating_border none

client.focused #CCCCCC #CCCCCC #CCCCCC #CCCCCC #CCCCCC
client.focused_inactive #222222 #222222 #222222 #222222 #222222
client.unfocused #222222 #222222 #222222 #222222 #222222

# Auto Start
exec mako

# run
include /etc/sway/config.d/*
